package com.example.multiscreenapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.multiscreenapp.databinding.ActivityMainBinding

class SecondActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            textView.text = "Activity 2"
            btnBack.setOnClickListener {
                finish()
            }
            btnForward.setOnClickListener {
                startActivity(Intent(this@SecondActivity, ThirdActivity::class.java))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("TAG", "onStart: $this" )
    }

    override fun onResume() {
        super.onResume()
        Log.e("TAG", "onResume: $this" )
    }

    override fun onPause() {
        super.onPause()
        Log.e("TAG", "onPause: $this" )
    }

    override fun onStop() {
        super.onStop()
        Log.e("TAG", "onStop: $this" )
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("TAG", "onDestroy: $this" )
    }
}