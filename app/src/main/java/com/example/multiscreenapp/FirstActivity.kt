package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.multiscreenapp.databinding.ActivityMainBinding


class FirstActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            textView.text = "Activity 1"
            btnBack.setOnClickListener {
                finish()
            }
            btnForward.setOnClickListener {
                startActivity(Intent(this@FirstActivity, SecondActivity::class.java))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("TAG", "onStart: $this" )
    }

    override fun onResume() {
        super.onResume()
        Log.e("TAG", "onResume: $this" )
    }

    override fun onPause() {
        super.onPause()
        Log.e("TAG", "onPause: $this" )
    }

    override fun onStop() {
        super.onStop()
        Log.e("TAG", "onStop: $this" )
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("TAG", "onDestroy: $this" )
    }
}